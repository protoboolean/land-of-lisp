(defparameter *nodes*
  '((living-room
     (you are in the living-room. a wizard is snoring loudly on the couch.))
    (garden
     (you are in a beautiful garden. there is a well in front of you.))
    (attic
     (you are in the attic. there is a giant welding torch in the corner.))))

(defparameter *edges*
  '((living-room
     (garden west door)
     (attic upstairs ladder))
    (garden
     (living-room east door))
    (attic
     (living-room downstairs ladder))))

(defparameter *objects*
  '(whiskey bucket frog chain))

(defparameter *object-locations*
  '((whiskey living-room)
    (bucket living-room)
    (chain garden)
    (frog garden)))

(defparameter *location*
  'living-room)

(defparameter *allowed-commands*
  '(look walk pickup inventory))

(defun describe-location (location-id locations)
  (first (rest (assoc location-id locations))))

(defun describe-path (edge)
  `(there is a ,(nth 2 edge) going ,(nth 1 edge) from here.))

(defun describe-paths (location edges)
  (let ((paths (rest (assoc location edges))))
    (apply #'append (mapcar #'describe-path paths))))

(defun objects-at (location objects object-locations)
  (labels ((object-location (object)
	     (nth 1 (assoc object object-locations)))
	   (at-loc-p (object)
	     (eq location (object-location object))))
    (remove-if-not #'at-loc-p objects)))

(defun describe-objects (location objects object-locations)
  (labels ((describe-object (object)
	     `(you see a ,object on the floor.)))
    (apply #'append (mapcar #'describe-object (objects-at location objects object-locations)))))

(defun look ()
  (append (describe-location *location* *nodes*)
	  (describe-paths *location* *edges*)
	  (describe-objects *location* *objects* *object-locations*)))

(defun walk (direction)
  (let ((next-edge
	 (find direction (rest (assoc *location* *edges*))
	       :key (lambda (edge) (nth 1 edge)))))
    (if next-edge
	(progn
	  (setf *location* (first next-edge))
	  (look))
	'(you cannot go that way.))))

(defun pickup (object)
  (cond ((member object
		 (objects-at *location* *objects* *object-locations*))
	 (push `(,object body) *object-locations*)
	 `(you are now carrying the ,object))
	(t `(you cannot get that.))))

(defun inventory ()
  (cons 'items- (objects-at 'body *objects* *object-locations*)))


(defun say-hello ()
  (princ "Please type your name: ")
  (let ((name (read-line)))
    (princ "Nice to meet you, ")
    (princ name)))

(defun game-repl ()
  (let ((cmd (game-read)))
    (unless (eq (car cmd) 'quit)
      (game-print (game-eval cmd))
      (game-repl))))

(defun game-read ()
  (let ((cmd (read-command (read-line))))
    (flet ((quote-it (x)
	     (list 'quote x)))
      (cons (car cmd) (mapcar #'quote-it (cdr cmd))))))

(defun game-eval (sexp)
  (if (member (car sexp) *allowed-commands*)
      (eval sexp)
      '(i do not know that command.)))

(defun read-command (input-string)
  (read-from-string
   (concatenate 'string "(" input-string ")")))

(defun tweak-text (lst caps lit)
  (when lst
    (let ((item (first lst))
	  (rest (rest  lst)))
      (cond ((eql item #\space) (cons item (tweak-text rest caps lit)))
	    ((member item '(#\| #\? #\.)) (cons item (tweak-text rest t lit)))
	    ((eql item #\") (tweak-text rest caps (not lit)))
	    (lit (cons item (tweak-text rest nil lit)))
	    (caps (cons (char-upcase item) (tweak-text rest nil lit)))
	    (t (cons (char-downcase item) (tweak-text rest nil nil)))))))

(defun to-string (object) (coerce object 'string))
(defun to-list   (object) (coerce object 'list))

(defun game-print (lst)
  (let ((normalised-text (string-trim "() " (prin1-to-string lst))))
    (princ (to-string (tweak-text (to-list normalised-text) t nil)))
    (fresh-line)))

